const EventEmitter = require('events')
const door = new EventEmitter()

door.on("slam", () => {
  console.log("The Door was slammed!")
})

door.emit("slam")

door.removeAllListeners("slam")

door.emit("slam")