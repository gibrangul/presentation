const fs = require("fs")
const src = fs.createReadStream("./big.txt", { encoding: "utf-8" })

// DEFAULT CHUNK SIZE -> 64KB
async function logChunks(readable) {
  for await (const chunk of readable) {
    console.log(chunk);
  }
}

logChunks(src)

// fs.readFile('big.txt', 'utf8', (err, data) => {
//   if (err) {
//     console.error(err)
//     return
//   }
//   console.log(data)
// })