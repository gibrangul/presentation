const fs = require('fs')

const content = 'Some content!'

fs.writeFile('new.txt', content, err => {
  if (err) {
    console.error(err)
    return
  }
  console.log("File written successfully")
})