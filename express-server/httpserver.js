var http = require('http');

//create a server object:

http.createServer(function (req, res) {
  res.writeHead(200, { 'Content-Type': 'text/html' });
  var url = req.url;
  console.log(req.method)
  if (url === '/') {
    res.write('Hello World!!!');
    res.end();
  } else if (url === '/otherside') {
    res.write('Hello from the other side!');
    res.end();
  }
}).listen(8000, function () {
  console.log("server started on port 8000"); //the server object listens on port 3000
});